function [Y,U,psnrUI,matriceIntermediara,paletaCulori,hufRate]=main(I, X, M, numOfColors)

matriceIntermediara=zerYYos(512,512);
paletaCulori=ones(512,512);
Y=problema1(I,X);
%Y=0;

[U,psnrUI]=problema2(I,M,numOfColors);
%U=0;
%psnrUI=0;

hufRate=0;

end
