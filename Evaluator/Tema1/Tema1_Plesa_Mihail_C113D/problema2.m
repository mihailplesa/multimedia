function [U, psnrUI] = problema2(I,M,numOfColors)
[X, map]=rgb2ind(I,numOfColors);
U=ind2rgb(X,map);
U=im2uint8(U);
psnrUI=psnr(U,I);
end