function [Y] = problema1 (I,X)
Y=X;
valI=I(:);
valX=X(:);
valY=Y(:);
n=length(valI);
for i=1:n
    for p=1:8
        bI=bitget(valI(i),p);
        bX=bitget(valX(i),p);
        if bI==bX && bI==1
            valY(i)=bitset(valY(i),p,0);
        end
    end
end
Y=reshape(valY,size(I));
end