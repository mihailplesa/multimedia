function lungime=depthfirst(index,depth)
    global T; 
    global LL;
    if(isempty(T(index).children))  
         LL(T(index).symbol)=depth;
         lungime=T(index).value*depth;
    else
        lungime=0;
        l=length(T(index).children);
        for i=1:l
            child=T(index).children(i);
            lungime=lungime+depthfirst(child,depth+1);
        end        
    end  
end