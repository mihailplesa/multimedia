clear all;
close all;
clc;

%I=imread('fruits512.bmp');
%I=imread('baboon.bmp');
I=imread('lena512color.bmp');
%I=imread('atm512color.bmp');
%I=imread('w.bmp');

e1=entropy(uint8(I));

X=imread('lena512color.bmp');

M=[0 8 2 10; 12 4 14 6; 3 11 1 9; 15 7 13 5];
M=M/16;

numOfColors=512;

[Y,U,psnrUI,hufRate]=main(I,X,M,numOfColors);
%imshow(U)
%psnrUI
hufRate