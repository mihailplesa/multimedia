function Y=sarcina1(I,X)

m=size(I,1);
n=size(I,2);

A=dec2bin(I,8)-'0';
B=dec2bin(X,8)-'0';
C=bitand(not(A),B);

Y=bin2dec(char(C+'0'));
Y=reshape(Y,m,n,3);
 
Y=uint8(Y);

end