function [U,psnrUI,matriceIntermediara,paletaCulori]=sarcina2(I,M,numOfColors)

m1=size(I,1);
n1=size(I,2);

m=size(M,1);
n=size(M,2);

% N=round(numOfColors^(1/3));
N=numOfColors;
r=[256/(N-1) 256/(N-1) 256/(N-1)];

J=double(I);

color=zeros(1,3);

for x=1:m1
    for y=1:n1
        spread=r*(M(mod(x-1,m)+1,mod(y-1,n)+1)-1/2);
        color(1)=double(I(x,y,1));
        color(2)=double(I(x,y,2));
        color(3)=double(I(x,y,3));
        J(x,y,:)=color+spread;
    end
end

matriceIntermediara=uint8(J);
minJ=min(J(:));
maxJ=max(J(:));
J=(J-minJ)/(maxJ-minJ);
% matricea intermediara


% paleta de culori (in [0-1])
[IDX,MAP] = rgb2ind(I,numOfColors,'n');
paletaCulori=MAP;

% transformarea conform paletei (nearest color)
F=rgb2ind(J,MAP,'nodither');

% din imaginea indexata in cea RGB
U=ind2rgb(F,MAP);
U=uint8(U*255);

psnrUI=psnr(U,I);

end
