close all;
clear all;
clc;

%%%%%%%%%%%fixez rezultatele corecte%%%%%%%%%%%%%%%%%%%%%%
I=imread('Imagini/Mac0_OG0.s.ppm');
X=imread('Imagini/Stat0_OG3.s.ppm');
M=1/9*[0 7 3;6 5 2;4 1 8];
numOfColors=32;
currentFolder=cd('TemaEtalon/'); %calea catre folderul unde este scriptul etalon
[Y_et,U_et,psnrUI_et,matriceIntermediara_et,paletaCulori_et,hufRate_et]=main(I,X,M,numOfColors);
cd(currentFolder);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%Incepe evaluarea studentilor ^_^ %%%%%%%%%%%%%%%%%
folder_students='Tema1';
[folder_names]=dir(folder_students); %folderul unde se afla folderele cu teme
num_of_folders=size(folder_names,1);

%%%%%%%% pentru linux sterg numele directoarelor . si .. %%%%%%%%%
cnt=0;
index_to_eliminate=[];
for i=1:num_of_folders
    if strcmp(folder_names(i).name, '.')==1 || strcmp(folder_names(i).name,'..')==1
        cnt=cnt+1;
        index_to_eliminate(cnt)=i;
    end
end
folder_names(index_to_eliminate)=[];

numOfStudents=size(folder_names,1);
scores=zeros(1,numOfStudents);
revision=zeros(1,numOfStudents);
psnrStudents=zeros(numOfStudents);
for i=1:numOfStudents
    homeworkPath=strcat(folder_students,'/',folder_names(i).name);
    currentFolder=cd(homeworkPath);
    try
        [Y,U,psnrUI,matriceIntermediara,paletaCulori,hufRate]=main(I,X,M,numOfColors);
        if isequal(Y,Y_et)==1
            scores(i)=scores(i)+1;
        end
        scores(i)=scores(i)+min(hufRate/hufRate_et,1);
        %retin valoarea psnrUI (daca ea este egala cu cea calculata de noi) ca sa fac apoi maximul si raportul
        psnrStudents(i)=psnrUI;
        if psnrUI ~= psnr(U,I) || isequal(matriceIntermediara,matriceIntermediara_et) == 0
            revision(i)=1;
        end
    catch
        revision(i)=2;
    end
    cd(currentFolder);
end

%calculez punctajele finale luand in calcul si psnr-ul relativ la valoarea
%psnrUI_et (32.3253)
for i=1:numOfStudents
    scores(i)=scores(i)+min(1.0,psnrStudents(i)/psnrUI_et);
end



%%%%%%%%%%extrag datele despre studenti si fac tabelul cu note%%%%%%%%%%%%%%%%%%%
numOfStudents=size(folder_names,1);
students_names=extractfield(folder_names,'name');
%%%%tabela cu notele%%%%
grades=cell2table(cell(0,5), 'VariableNames', {'NrCrt', 'Nume', 'Prenume', 'Grupa','Nota'});

%%%%%%%%%%Printez studentii care au nevoie de notare manuala a codului %%%%
%cei calora psnrul dat de ei nu este cel recalculat de noi
%cei care au alta matrice intermediara
fileID=fopen('manual_revision.txt','w');

for i=1:numOfStudents
    studentData=strsplit(cell2mat(students_names(i)),'_');
    studentResult={sprintf('%d',i),studentData(2),studentData(3),studentData(4),sprintf('%.2f',scores(i))};
    if revision(i)==2
        fprintf(fileID,'Student %s %s from %s (code not running)\n',cell2mat(studentData(2)),cell2mat(studentData(3)),cell2mat(studentData(4)));
    else
        if revision(i)==1
            fprintf(fileID,'Student %s %s from %s (problems with task 2)\n',cell2mat(studentData(2)),cell2mat(studentData(3)),cell2mat(studentData(4)));
        end
    end
    grades=[grades;studentResult];
end
writetable(grades,'grades.xls');



