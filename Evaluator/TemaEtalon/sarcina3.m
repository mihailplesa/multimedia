function hufRate=sarcina3(I)

global T;
global LL;

LL=zeros(1,256);

% I=zeros(8,8);
% I(:,5:8)=0;
% I(:,1)=1;
% I(:,2)=2;
% I(:,3)=3;
% I(:,4)=4;
% 
% m=size(I,1);
% n=size(I,2);

IV=I(:);

bins=0:255;
H=hist(IV,bins);

nb=0;
for i=1:256
    if(H(i)>0)
        nb=nb+1;
        T(nb).symbol = i;
        T(nb).value = H(i);
        T(nb).children = [];
    end
end

left=1;
right=nb;
while(nb>0)
    table = struct2table(T(left:right)); % convert the struct array to a table
    sortedtable = sortrows(table, 'value'); % sort the table by value
    T(left:right) = table2struct(sortedtable); % change it back to struct array if necessary
    
    index=right+1;
    if(right-left<=3)
        T(index).value = 0;
        T(index).children = [];        
        for i=left:right
            T(index).value=T(index).value + T(i).value;  
            T(index).children = [T(index).children , i];
        end        
        nb=0;
    else
        T(index).value = 0;
        T(index).children = [];        
        for i=left:left+3
            T(index).value=T(index).value + T(i).value;  
            T(index).children = [T(index).children , i];
        end        
        left=left+4;
        right=right+1;
        nb=right-left+1;
    end
end

lung=depthfirst(index,0);

hufRate=(length(IV)*4)/lung;

end