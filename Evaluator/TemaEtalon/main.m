function [Y,U,psnrUI,matriceIntermediara,paletaCulori,hufRate]=main(I, X, M, numOfColors)

Y=sarcina1(I,X);
%Y=0;

[U,psnrUI,matriceIntermediara,paletaCulori]=sarcina2(I,M,numOfColors);
% U=0;
% psnrUI=0;
% matriceIntermediara=0;
% paletaCulori=0;

hufRate=sarcina3(I);
%hufRate=0;

end