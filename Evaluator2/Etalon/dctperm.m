function JM=dctperm(IM,s,f)
    JM=IM;
    k=size(s,2);
    % pe cele 3 canale R, G si B
    for i=1:3
        I=IM(:,:,i);
        I=double(I);
        D=dct2(I);
        DV=zeros(1,k);
        % amplificarea coeficientilor DCT cu f
        for j=1:k
            DV(j)=D(j+1,j+1)*f;
        end
        DVP=DV;
        for j=1:k
            DVP(j)=DV(s(j));
            D(j+1,j+1)=DVP(j);
        end
        J=idct2(D);
        J=uint8(J);
        JM(:,:,i)=J;
    end  
end