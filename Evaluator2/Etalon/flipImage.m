function V=flipImage(I)
    m=size(I,1);
    n=size(I,2);
    V=zeros(8,m*n/8);
    k=0;
    for i=1:m/8
        for j=1:n/8
            if(mod(i,2)==1)
                B=I((i-1)*8+1:i*8,(j-1)*8+1:j*8);            
            else
                B=I((i-1)*8+1:i*8,(n/8-j)*8+1:(n/8-j+1)*8);            
            end
            V(:,k*8+1:k*8+8)=B;
            k=k+1;
        end
    end
end