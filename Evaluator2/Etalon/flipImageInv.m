function I=flipImageInv(V,m,n)
    I=zeros(m,n);
    k=0;
    for i=1:m/8
        for j=1:n/8
            B=V(:,k*8+1:k*8+8);
            k=k+1;
            if(mod(i,2)==1)
                I((i-1)*8+1:i*8,(j-1)*8+1:j*8)=B;            
            else
                I((i-1)*8+1:i*8,(n/8-j)*8+1:(n/8-j+1)*8)=B;            
            end
        end
    end
end