function quadcompDFGray(x,y,level)
   global I1
   global J1
   global nbBiti
   global lastLevel
   global delta
   m=size(I1,1)/(2^level);
   B=I1(x+1:x+m,y+1:y+m);
   s=std(B(:));
   med=mean(B(:));
   if(s<=delta)
       if(lastLevel ~= level)
           nbBiti=nbBiti+level-lastLevel;
           lastLevel=level;
       end
       nbBiti=nbBiti+9;
       J1(x+1:x+m,y+1:y+m)=ones(m,m)*med;             
   else
       quadcompDFGray(x,y,level+1);
       quadcompDFGray(x,y+m/2,level+1);
       quadcompDFGray(x+m/2,y+m/2,level+1);
       quadcompDFGray(x+m/2,y,level+1);
       lastLevel=lastLevel-1;
   end
end