function B=jpegblockinv(D,quality,Q)
    if quality<50
        scale=50/quality;
    else
        scale=2-quality/50;
    end

    D=round(D.*(scale*Q));
    B=idct2(D);     
end