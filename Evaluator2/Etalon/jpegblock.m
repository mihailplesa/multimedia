function D=jpegblock(B,quality,Q)
    if quality<50
        scale=50/quality;
    else
        scale=2-quality/50;
    end

    D=dct2(B); 
    D=round(D./(scale*Q));
end