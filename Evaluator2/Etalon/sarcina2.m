function K=sarcina2(fileName,quality)
    
    QY=[16 11 10 16 124 140 151 161;
    12 12 14 19 126 158 160 155;
    14 13 16 24 140 157 169 156;
    14 17 22 29 151 187 180 162;
    18 22 37 56 168 109 103 177;
    24 35 55 64 181 104 113 192;
    49 64 78 87 103 121 120 101;
    72 92 95 98 112 100 103 199] ;

    QC=[17 18 24 47 99 99 99 99;
    18 21 26 66 99 99 99 99 ;
    24 26 56 99 99 99 99 99 ;
    47 66 99 99 99 99 99 99 ;
    99 99 99 99 99 99 99 99 ;
    99 99 99 99 99 99 99 99 ;
    99 99 99 99 99 99 99 99 ;
    99 99 99 99 99 99 99 99];

    fileID = fopen(fileName,'r');
    m=fread(fileID,1,'uint16');
    n=fread(fileID,1,'uint16');
    
    I=zeros(m,n,3);        
    Y=decompressJPEG(I,fileID,QY,quality);
    
    I=zeros(m/2,n/2,3);        
    CB=decompressJPEG(I,fileID,QC,quality);    
    CB=imresize(CB,2);
    
    CR=decompressJPEG(I,fileID,QC,quality);
    CR=imresize(CR,2);
    fclose(fileID);
    
    YCBCR=zeros(m,n,3);
    YCBCR(:,:,1)=Y;
    YCBCR(:,:,2)=CB;
    YCBCR(:,:,3)=CR;
    YCBCR=uint8(YCBCR);      
    
    K=ycbcr2rgb(YCBCR);
end