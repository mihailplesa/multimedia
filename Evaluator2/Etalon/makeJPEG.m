clear all; close all;
% producerea fisierului prin compresia JPEG : jpeg.bin
I=imread('lion.bmp');
%I=imread('fruits512.bmp');
%I=imread('color1.bmp');


QY=[16 11 10 16 124 140 151 161;
    12 12 14 19 126 158 160 155;
    14 13 16 24 140 157 169 156;
    14 17 22 29 151 187 180 162;
    18 22 37 56 168 109 103 177;
    24 35 55 64 181 104 113 192;
    49 64 78 87 103 121 120 101;
    72 92 95 98 112 100 103 199] ;

QC=[17 18 24 47 99 99 99 99;
    18 21 26 66 99 99 99 99 ;
    24 26 56 99 99 99 99 99 ;
    47 66 99 99 99 99 99 99 ;
    99 99 99 99 99 99 99 99 ;
    99 99 99 99 99 99 99 99 ;
    99 99 99 99 99 99 99 99 ;
    99 99 99 99 99 99 99 99];

quality=25;

m=size(I,1); 
n=size(I,2); 
fileID = fopen('lion.bin','w');
fwrite(fileID,m,'uint16');
fwrite(fileID,n,'uint16');

YCBCR=rgb2ycbcr(I);

Y=YCBCR(:,:,1);
compressJPEG(Y,fileID,QY,quality);

CB=YCBCR(:,:,2);CB=imresize(CB,0.5);
compressJPEG(CB,fileID,QC,quality);

CR=YCBCR(:,:,3);CR=imresize(CR,0.5);
compressJPEG(CR,fileID,QC,quality);

fclose(fileID);