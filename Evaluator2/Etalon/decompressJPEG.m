function J=decompressJPEG(I,fileID,Q,quality)
    m=size(I,1);
    n=size(I,2);
       
    minimum1=fread(fileID,1,'int16');    
    maximum1=fread(fileID,1,'int16');
    F1=fread(fileID,(maximum1-minimum1+1),'uint16');
    F1=F1';
    
    L = fread(fileID,1,'uint32'); 
    codBinar = fread(fileID,L,'ubit1');   
    codBinar=codBinar';
    
    probas1=F1/sum(F1);
    dict1 = huffmandict(minimum1:maximum1,probas1); % crearea dictionarului Huffman.
    vectorDC1 = huffmandeco(codBinar,dict1); % decodarea codului Huffman 
   
    DV1=zeros(8,m*n/8);
    DV1(1,1)=vectorDC1(1);
    for i=2:m*n/64
        DV1(1,(i-1)*8+1)=DV1(1,(i-2)*8+1)+vectorDC1(i);       
    end
    
    global index
    Z=ZigzagOrder(8);
    [dummy index]=sort(Z(:));
    RLE=[];
    for i=1:m*n/64
        RLE=[RLE codRLE(DV1(:,(i-1)*8+1:i*8))];
    end
    
    minimum=fread(fileID,1,'int16'); 
    maximum=fread(fileID,1,'int16');
    F=fread(fileID,maximum-minimum+1,'int16');
    lenRLE=fread(fileID,1,'uint32'); 
    lenCA=fread(fileID,1,'uint32');    
    codCA=fread(fileID,lenCA,'ubit1');    

    RLE=arithdeco(codCA,F,lenRLE);
    RLE=RLE+minimum-1;
    
    global index
    Z=ZigzagOrder(8);
    [dummy index]=sort(Z(:));
        
    pozZeros=find(RLE==0);  
    blocRLE=[];
    if(pozZeros(1)>1)
        blocRLE=RLE(1:pozZeros(1)-1);
    end
    if(~isempty(blocRLE))
        DC=DV1(1,1);
        DV1(:,1:8)=decodRLE(blocRLE);
        DV1(1,1)=DC;
    end
      
    for i=2:m*n/64
        blocRLE=[];
        if(pozZeros(i)>(pozZeros(i-1)+1))
            blocRLE=RLE(pozZeros(i-1)+1:pozZeros(i)-1);
        end
        if(~isempty(blocRLE))
            DC=DV1(1,(i-1)*8+1);
            DV1(:,(i-1)*8+1:i*8)=decodRLE(blocRLE);
            DV1(1,(i-1)*8+1)=DC;
        end
    end
        
    JV=DV1;
    for i=1:m*n/64
        D=DV1(:,(i-1)*8+1:i*8);
        B=jpegblockinv(D,quality,Q);
        JV(:,(i-1)*8+1:i*8)=B;
    end
           
    JV=JV+128;
    JV=uint8(JV);
    J=flipImageInv(JV,m,n);    
end