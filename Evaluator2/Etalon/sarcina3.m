function [rateX,psnrX]=sarcina3(X,variation)
    global I1
    global J1
    global nbBiti
    global delta 
    global lastLevel
    
    delta=variation;
    nbBitiTotal=0;

    n=size(X,1); 
    YCBCR=rgb2ycbcr(X);

    for i=1:3        
        I1=YCBCR(:,:,i);        
%         I1=[192 192 255 255;
%             127 192 255 255;
%             127 127 192 255;
%             127 127 127 192];
        I1=double(I1);
        J1=I1;
        nbBiti=0;              
        
        lastLevel=0;
        quadcompDFGray(0,0,0);
        
        YCBCR(:,:,i)=uint8(J1);
        nbBitiTotal=nbBitiTotal+nbBiti;
    end

    Y=ycbcr2rgb(YCBCR);
    rateX=(n*n*24)/nbBitiTotal;
    psnrX=psnr(X,Y);
    
%     subplot(1,2,1); imshow(X), title('Original');
%     subplot(1,2,2); imshow(Y), title(sprintf('Rata=%.2f PSNR=%.2f',rateX,psnrX));
    
end