% I grascale image
function compressJPEG(I,fileID,Q,quality)
    m=size(I,1);
    n=size(I,2);
    
    IV=flipImage(I);       
    IV=double(IV);
    IV=IV-128;
    DV=IV;
    for i=1:m*n/64
        B=IV(:,(i-1)*8+1:i*8);
        D=jpegblock(B,quality,Q);
        DV(:,(i-1)*8+1:i*8)=D;
    end

%     TJ=flipImageInv(DV,m,n); 
%     DCM=zeros(m/8,n/8);
%     for i=1:m/8
%         for j=1:n/8
%             DCM(i,j)=TJ((i-1)*8+1,(j-1)*8+1);
%         end
%     end
%     DCM=round(DCM);
%     imshow(DCM,[]);
    
    vectorDC=zeros(1,m*n/64);
    vectorDC(1)=DV(1,1);
    for i=2:m*n/64
        vectorDC(i)=DV(1,(i-1)*8+1)-DV(1,(i-2)*8+1);
    end
    
    countSymbols=m*n/64;
    minimum=min(vectorDC);
    maximum=max(vectorDC);
    F=hist(vectorDC,minimum:maximum);   
    probas=F/countSymbols;    
    dict = huffmandict(minimum:maximum,probas); % crearea dictionarului Huffman.
    codHuffman = huffmanenco(vectorDC,dict); % Encode the data.
    
    fwrite(fileID,minimum,'int16');    
    fwrite(fileID,maximum,'int16');
    fwrite(fileID,F,'uint16');
    
    len=length(codHuffman);
    fwrite(fileID,len,'uint32');    
    fwrite(fileID,codHuffman,'ubit1');    
    
    % compresie coeficienti AC
    global index
    Z=ZigzagOrder(8);
    [dummy index]=sort(Z(:));
    RLE=[];
    for i=1:m*n/64
        RLE=[RLE codRLE(DV(:,(i-1)*8+1:i*8))];
    end
    
    minimum=min(RLE);
    maximum=max(RLE);
    RLE=RLE-minimum+1;
    F=hist(RLE,1:maximum-minimum+1);   
    F=F+1;
    
    codCA = arithenco(RLE,F); 
    fwrite(fileID,minimum,'int16');    
    fwrite(fileID,maximum,'int16');
    fwrite(fileID,F,'uint16');
    fwrite(fileID,length(RLE),'uint32');
    
    len=length(codCA);
    fwrite(fileID,len,'uint32');    
    fwrite(fileID,codCA,'ubit1');    
end