clear all;
close all;
clc;

% date pentru sarcina 1
I=imread('permDCT.bmp');
rand('seed',0);
k=100;
perm=randperm(k);
scale=2;

%date pentru sarcina 2
fileName='lion.bin';
quality=25;

%fileName='jpeg.bin';
%quality=50;

%date pentru sarcina 3
%X=imread('lena512color.bmp');
%variation=3;

X=imread('nature.bmp');
variation=5;

[J,K,rateX,psnrX]=main(I,perm,scale,fileName,quality,X,variation);

% test sarcina 1
% O=imread('halep.bmp');
% subplot(1,3,1);
% imshow(O),title('Originala');
% subplot(1,3,2);
% imshow(I),title('Transformata');
% subplot(1,3,3);
% imshow(J),title('Reconstruita');
% return

% test sarcina 2
% O=imread('lion.bmp');
% psnrK=psnr(O,K);
% subplot(1,2,1);
% imshow(O),title('Originala');
% subplot(1,2,2);
% imshow(K), title(sprintf('PSNR=%.2f',psnrK));
% return

% test sarcina 3 - vezi in scriptul de la sarcina 3