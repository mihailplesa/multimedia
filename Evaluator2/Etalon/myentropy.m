function ent=myentropy(I)
    I=double(I);
    minimum=min(I(:));
    maximum=max(I(:));
    ent=0;
    F=size(I(:),1);
    nbBins=maximum-minimum+1;
    N=hist(I(:),nbBins);
    for i=minimum:maximum
        pi=N(i-minimum+1)/F;
        if(pi~=0)
            ent=ent+pi*log2(1/pi);
        end
    end
end