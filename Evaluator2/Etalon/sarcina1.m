function JM=sarcina1(IM,perm,f)
    k=size(perm,2);    
    iperm=perm;
    for i=1:k
        iperm(perm(i))=i;
    end
    
    JM=IM;
    
    for i=1:3
        I=IM(:,:,i);
        I=double(I);
        D=dct2(I);
        DV=zeros(1,k);
        for j=1:k
            DV(j)=D(j+1,j+1)/f;
        end
        DVP=DV;
        for j=1:k
            DVP(j)=DV(iperm(j));
            D(j+1,j+1)=DVP(j);
        end
        J=idct2(D);
        J=uint8(J);
        JM(:,:,i)=J;
    end  
end