function RLE=codRLE(DCTQ)
    global index
    DCTQV=DCTQ(index(2:64));
    DCTQV=DCTQV(:)';
    
    NZ=find(DCTQV~=0);    
    l=length(NZ);
    RLE=zeros(1,l+1);
    if(l>0)
        R=NZ(1)-1;
        L=DCTQV(NZ(1));
        RLE(1)=L*64+R;
    end
    for i=2:l
        R=NZ(i)-NZ(i-1)-1;
        L=DCTQV(NZ(i));
        RLE(i)=L*64+R;
    end  
    RLE(l+1)=0;
end
