clear all; close all; clc
% producerea imaginii transformate prin permutare DCT
%IM=imread('lena512color.bmp');
IM=imread('halep.bmp');

rand('seed',0);
k=100;
s=randperm(k);

f=2;
IMP=dctperm(IM,s,f);

subplot(1,3,1);
imshow(IM), title('Imaginea Originala');
subplot(1,3,2);
imshow(IMP), title('Imaginea Transformata');

imwrite(IMP,'permDCT.bmp');

k=size(s,2);
% se calculeaza permutarea inversa
si=s;
for i=1:k
    si(s(i))=i;
end

IMP1=dctperm(IMP,si,1/f);

subplot(1,3,3);
imshow(IMP1),title('Imaginea Reconstruita');

psnr(IM,IMP1)