function DCTQ=decodRLE(RLE)
    global index
    DCTQV=zeros(1,63);
    l=length(RLE);
    k=1;
    for i=1:l
        R=mod(RLE(i),64);
        L=(RLE(i)-R)/64;
        for j=1:R
            DCTQV(k)=0;    
            k=k+1;
        end
        DCTQV(k)=L;
        k=k+1;
    end
    
    DCTQ=zeros(8,8);
    DCTQ(index(2:64))=DCTQV;
end
