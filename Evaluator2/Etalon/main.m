function [J,K,rateX,psnrX]=main(I,perm,scale,fileName,quality,X,variation)

J=sarcina1(I,perm,scale);
%J=0;

K=sarcina2(fileName,quality);
%K=0;

[rateX,psnrX]=sarcina3(X,variation);
%rateX=0;
%psnrX=0;

end