close all;
clear all;
clc;

%%%%%%%%%%%fixez rezultatele corecte%%%%%%%%%%%%%%%%%%%%%%
I=imread('Etalon/permDCT.bmp');
rand('seed',0);
k=100; perm=randperm(k);
scale=2;

fileName='Etalon/lion.bin';
quality=25;

X=imread('Etalon/nature.bmp');
stddev=5;


currentFolder=cd('Etalon/'); %calea catre folderul unde este scriptul etalon
[J_et,K_et,rateX_et,psnrX_et]=main(I, perm, scale, fileName, quality, X, stddev);
psnrEvalP1=psnr(J_et,I);
O=imread('Etalon/lion.bmp');
psnrEvalP2=psnr(K_et,O);
cd(currentFolder);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%Incepe evaluarea studentilor ^_^ %%%%%%%%%%%%%%%%%
folder_students='Tema_2_C113D';
[folder_names]=dir(folder_students); %folderul unde se afla folderele cu teme
num_of_folders=size(folder_names,1);

%%%%%%%% pentru linux sterg numele directoarelor . si .. %%%%%%%%%
cnt=0;
index_to_eliminate=[];
for i=1:num_of_folders
    if strcmp(folder_names(i).name, '.')==1 || strcmp(folder_names(i).name,'..')==1
        cnt=cnt+1;
        index_to_eliminate(cnt)=i;
    end
end
folder_names(index_to_eliminate)=[];

numOfStudents=size(folder_names,1);
scores=zeros(1,numOfStudents);
cheater=zeros(1,numOfStudents);
psnrStudents=zeros(numOfStudents);
for i=1:numOfStudents
    homeworkPath=strcat(folder_students,'/',folder_names(i).name);
    currentFolder=cd(homeworkPath);
    try
        fileName='../../Etalon/lion.bin';
        [J,K,rateX,psnrX]=main(I, perm, scale, fileName, quality, X, stddev);
        J=uint8(J);
        K=uint8(K);
        psnrStudentP1=psnr(J,I);
        scores(i)=scores(i)+(psnrStudentP1/psnrEvalP1); %punctaj 1

        psnrStudentP2=psnr(K,O);
        scores(i)=scores(i)+2*(psnrStudentP2/psnrEvalP2);

        rap1=rateX/rateX_et;
        rap2=psnrX/psnrX_et;
        scores(i)=scores(i)+(rap1+rap2)/2;
    catch
        scores(i)=-1;
    end
    cd(currentFolder);
end


%%%%%%%%%%extrag datele despre studenti si fac tabelul cu note%%%%%%%%%%%%%%%%%%%
numOfStudents=size(folder_names,1);
students_names=extractfield(folder_names,'name');
%%%%tabela cu notele%%%%
grades=cell2table(cell(0,5), 'VariableNames', {'NrCrt', 'Nume', 'Prenume', 'Grupa','Nota'});

for i=1:numOfStudents
    studentData=strsplit(cell2mat(students_names(i)),'_');
    studentResult={sprintf('%d',i),studentData(2),studentData(3),studentData(4),sprintf('%.2f',scores(i))};
    if cheater(i)==1
        disp(sprintf('The student %s %s is a cheater!!!',cell2mat(studentData(2)),cell2mat(studentData(3))));
    end
    grades=[grades;studentResult];
end

writetable(grades,'grades.xls');

